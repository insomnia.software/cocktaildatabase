﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Cocktail_Database
{
    public class IngredientsUI : INotifyPropertyChanged
    {
        public event StatSelected OnStatSelected;

        private List<string> ingredientList = new List<string>();
        public List<string> IngredientList
        {
            get { return ingredientList; }
            set { ingredientList = value; }
        }
        private string selectedIngredient;
        public string SelectedIngredient
        {
            get { return selectedIngredient; }
            set
            {
                selectedIngredient = value;
                OnPropertyChanged("SelectedStat");

                if (value != "Select")
                {
                    AmountTextBoxEnabled = true;
                    SelectedEventArgs e = new SelectedEventArgs();
                    e.add = true;
                    OnStatSelected(this, e);
                }
                else
                {
                    AmountTextBoxEnabled = false;
                    //ComparativeValue = 0;
                    SelectedEventArgs e = new SelectedEventArgs();
                    e.add = false;
                    OnStatSelected(this, e);
                }
            }
        }

        private string amount;
        public string Amount
        {
            get { return amount; }
            set
            {
                amount = value;
                OnPropertyChanged("Amount");
            }
        }
        private bool amountTextBoxEnabled;
        public bool AmountTextBoxEnabled
        {
            get { return amountTextBoxEnabled; }
            set
            {
                amountTextBoxEnabled = value;
                OnPropertyChanged("AmountTextBoxEnabled");
            }
        }

        public IngredientsUI()
        {
            string lol = Cocktail_Database.Properties.Settings.Default.ListOfIngredients;

            string[] lolarray = lol.Split(',');

            foreach (var item in lolarray)
            {
                ingredientList.Add(item);
            }

            selectedIngredient = "Select"; //uses the lowercase version to prevent triggeting the set method on initiation of the control
            AmountTextBoxEnabled = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(
                    this, new PropertyChangedEventArgs(propName));
        }
    }

    public delegate void StatSelected(object sender, SelectedEventArgs e);

    public class SelectedEventArgs : EventArgs
    {
        public bool add;
    }
}
