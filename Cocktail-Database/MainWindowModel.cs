﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cocktail_Database
{
    public class MainWindowModel
    {
        MainWindowViewModel viewModel;
        MainWindow mainWindow;

        public void Start()
        {
            viewModel = new MainWindowViewModel();
            mainWindow = new MainWindow();
            mainWindow.DataContext = viewModel;
            mainWindow.InitializeComponent();
            mainWindow.Show();
        }
    }
}
