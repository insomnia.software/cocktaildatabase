﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace Cocktail_Database
{
    public partial class App : Application
    {
        private MainWindowModel mainWindowModel;

        public App()
        {
            mainWindowModel = new MainWindowModel();
            mainWindowModel.Start();
        }
    }
}
