﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cocktail_Database
{
    class Boundary
    {
        private Database db = new Database();

        public void AddCocktail(string name, string preperation, int glass, List<string[]> ingredientsWithAmount, bool IBA)
        {
            List<IngredientAndAmount> ingredientsAndAmounts = new List<IngredientAndAmount>();

            foreach (var item in ingredientsWithAmount)
            {
                ingredientsAndAmounts.Add(new IngredientAndAmount() { ingredient = GetIDFromName(item[0]),  amount = item[1]});   
            }

            db.AddCocktail(name, preperation, glass, ingredientsAndAmounts, IBA);
        }

        private int GetIDFromName(string name)
        {
            switch(name)
            {
                case "Vodka":
                    return 1;
                case "Tequila":
                    return 2;
                case "White rum":
                    return 3;
                case "Triple sec":
                    return 4;
                case "Gin":
                    return 5;
                case "Lemon juice":
                    return 6;
                case "Coke":
                    return 7;
                default:
                    return 0;
            }
        }
    }

    class IngredientAndAmount
    {
        public int ingredient { get; set; }
        public string amount { get; set; }
    }
}
