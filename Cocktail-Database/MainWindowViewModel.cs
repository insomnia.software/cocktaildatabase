﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows.Input;
using System.Diagnostics;

namespace Cocktail_Database
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        //private Database db = new Database();
        private Boundary boundary = new Boundary();

        public MainWindowViewModel()
        {
            IngredientsUI sf = new IngredientsUI();
            sf.OnStatSelected += new StatSelected(sf_OnStatSelected);
            Ingredients.Add(sf);

            glasses.Add("Select");
            glasses.Add("Collins");
            glasses.Add("Old Fashioned");
            glasses.Add("Highball");
            glasses.Add("Cocktail");
        }

        void sf_OnStatSelected(object sender, SelectedEventArgs e)
        {
            if (e.add)
            {
                IngredientsUI sf = new IngredientsUI();
                sf.OnStatSelected += new StatSelected(sf_OnStatSelected);
                Ingredients.Add(sf);
            }
            else
            {
                Ingredients.Remove(Ingredients[Ingredients.Count - 1]); //remove the last one
            }
        }

        private ObservableCollection<IngredientsUI> ingredients = new ObservableCollection<IngredientsUI>();
        public ObservableCollection<IngredientsUI> Ingredients
        {
            get { return ingredients; }
            set
            {
                ingredients = value;
                OnPropertyChanged("Ingredients");
            }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        private string preperation;
        public string Preperation
        {
            get { return preperation; }
            set
            {
                preperation = value;
                OnPropertyChanged("Preperation");
            }
        }

        private ObservableCollection<string> glasses = new ObservableCollection<string>();
        public ObservableCollection<string> Glasses
        {
            get { return glasses; }
            set
            {
                glasses = value;
                OnPropertyChanged("Glasses");
            }
        }

        private int selectedGlassesIndex;
        public int SelectedGlassesIndex
        {
            get { return selectedGlassesIndex; }
            set
            {
                selectedGlassesIndex = value;
                OnPropertyChanged("SelectedGlassesIndex");
            }
        }

        private bool isIBA;
        public bool IsIBA
        {
            get { return isIBA; }
            set
            {
                isIBA = value;
                OnPropertyChanged("IsIBA");
            }
        }

        private bool CanSave()
        {
            // Verify command can be executed here
            return true;
        }

        private void SaveObject()
        {
            // Save command execution logic
            List<string[]> ingredientsWithAmounts = new List<string[]>();

            foreach (var item in ingredients)
            {
                if (item.SelectedIngredient != "Select")
                {
                    string[] tmp = new string[2];
                    tmp[0] = item.SelectedIngredient;
                    tmp[1] = item.Amount;
                    ingredientsWithAmounts.Add(tmp);

                }
            }

            //db.AddCocktail(name, preperation, selectedGlassesIndex, ingredientsWithAmounts, isIBA);
            boundary.AddCocktail(name, preperation, selectedGlassesIndex, ingredientsWithAmounts, isIBA);

            //SearchResults = boundary.SearchByName(SearchText);
            //foreach (var item in SearchResults)
            //{
            //    item.PrintItem();
            //}
        }

        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        param => this.SaveObject(),
                        param => this.CanSave()
                    );
                }
                return _saveCommand;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(
                    this, new PropertyChangedEventArgs(propName));
        }
    }

    /// <summary>
    /// A command whose sole purpose is to 
    /// relay its functionality to other
    /// objects by invoking delegates. The
    /// default return value for the CanExecute
    /// method is 'true'.
    /// </summary>
    public class RelayCommand : ICommand
    {
        #region Fields

        readonly Action<object> _execute;
        readonly Predicate<object> _canExecute;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            _execute = execute;
            _canExecute = canExecute;
        }

        #endregion // Constructors

        #region ICommand Members

        [DebuggerStepThrough]
        public bool CanExecute(object parameters)
        {
            return _canExecute == null ? true : _canExecute(parameters);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameters)
        {
            _execute(parameters);
        }

        #endregion // ICommand Members
    }
}
