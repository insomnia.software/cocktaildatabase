﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;

namespace Cocktail_Database
{
    class Database
    {
        public void AddCocktail(string name, string preperation, int glass, List<IngredientAndAmount> ingredientsAndAmounts, bool IBA)
        {
            SQLiteConnection mySQLConnection = new SQLiteConnection(@"Data Source=C:\Cocktail-Database\Cocktail-Database\bin\Debug\CocktailDatabase.s3db");
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            string query1 = @"INSERT INTO Cocktails (Name, Preperation, Glass";

            string query2 = @", IBA) values ('" + name + "', '" + preperation + "', " + glass;

            int i = 1;
            foreach (var item in ingredientsAndAmounts)
            {
                query1 += ", Ingredient_" + i + ", Amount_" + i;
                query2 += ", " + ingredientsAndAmounts[0].ingredient + ", '" + ingredientsAndAmounts[0].amount + "'";
                i++;
            }

            query2 += ", '" + IBA + "');";

            string fullQuery = query1 + query2;

            //            SQLiteCommand mycommand = new SQLiteCommand(@"INSERT INTO Cocktails 
            //                                                        (Name, Preperation, Glass, Ingredient_1, Ingredient_2, Ingredient_3, Ingredient_4, Ingredient_5, Ingredient_6, Ingredient_7, Ingredient_8, 
            //                                                        Amount_1, Amount_2, Amount_3, Amount_4, Amount_5, Amount_6, Amount_7, Amount_8, IBA) 
            //                                                        values ('" + name + "', '" + preperation + "', " + glass + ", " + ingredientsAndAmounts[0].ingredient + ", " + ingredientsAndAmounts[1].ingredient + ", " + ingredientsAndAmounts[2].ingredient + ", " + ingredientsAndAmounts[3].ingredient + ", " + ingredientsAndAmounts[4].ingredient + ", " + ingredientsAndAmounts[5].ingredient + ", " + ingredientsAndAmounts[6].ingredient + ", " + ingredientsAndAmounts[7].ingredient + ", '" +
            //                                                                  ingredientsAndAmounts[0].amount + "', '" + ingredientsAndAmounts[1].amount + "', '" + ingredientsAndAmounts[2].amount + "', '" + ingredientsAndAmounts[3].amount + "', '" + ingredientsAndAmounts[4].amount + "', '" + ingredientsAndAmounts[5].amount + "', '" + ingredientsAndAmounts[6].amount + "', '" + ingredientsAndAmounts[7].amount + "', " + IBA + ");", mySQLConnection);
            SQLiteCommand mycommand = new SQLiteCommand(fullQuery, mySQLConnection);

            mycommand.ExecuteNonQuery();
        }
    }
}
